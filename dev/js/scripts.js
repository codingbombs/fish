var app = app || {};

var spBreak = 767;
var offsetY = window.pageYOffset;

app.init = function () {

  app.navigation();
  app.modal();
  app.slider();
  app.tabs();
  app.treeView();

};

app.isMobile = function () {

  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;

};

app.navigation = function () {
  var buttonOpen = $('.js-button-open');
  var overlay = $('.js-overlay');
  buttonOpen.click(function () {
    $(this).toggleClass('is-active');
    if ($(this).hasClass('is-active')) {
      $('.js-navigation').addClass('is-show-block');
      offsetY = window.pageYOffset;
      $('body').css({
        position: 'fixed',
        top: -offsetY + 'px',
        width: '100%',
      });
      overlay.stop().fadeIn();
    }
    return false;
  });

  $('.js-button-close, .js-overlay').click(function () {
    $('.js-navigation').removeClass('is-show-block');
    buttonOpen.removeClass('is-active');
    $('body')
      .css({
        top: 'auto',
        position: 'static',
        width: 'auto'
      });
    overlay.stop().fadeOut();
    $(window).scrollTop(offsetY);
    return false;
  });
};

// Modal
app.modal = function () {
  var offsetY = window.pageYOffset;
  if ($('.js-lightbox-popup').length) {
    var lightbox = $('.js-lightbox-popup');
    var btn = $('.js-open-lightbox');
    var modalInit = function () {

      btn.click(function () {
        var lightboxID = $(this).attr('data-lightbox');
        modalOpen(lightboxID);
      });

      lightbox.find('.js-popup-close').click(function () {
        var lightBox = $(this).parents('.js-lightbox-popup');
        modalClose(lightBox);
      });

    };

    var modalOpen = function (lightboxID) {

      $('.js-lightbox-popup[data-lightbox="' + lightboxID + '"]').fadeIn();
      $('.js-lightbox-popup[data-lightbox="' + lightboxID + '"]').scrollTop(0);
      offsetY = window.pageYOffset;
      $('body').addClass('is-no-scroll');
      $('body').attr('style', 'top:' + -offsetY + 'px !important');
      $(document).on('touchmove', function (e) {
        if ($(e.target).parents('.js-lightbox-window').length == 0) return false;
      });
    };

    var modalClose = function (lightBox) {
      $('body').removeClass('is-no-scroll');
      $('body').css({
        position: 'static',
        top: 'auto',
      });
      $(window).scrollTop(offsetY);
      lightBox.fadeOut('fast');
      $(document).off('touchmove');

    };
    modalInit();
  }

  $('.js-purchased').click( function () {
    $('.js-payment').hide();
    $('.js-congratulations').show();
  });
  $('.js-close-congratulations').click( function () {
    $('.js-popup-congratulations').fadeOut('fast');
    $('.js-payment').show();
    $('.js-congratulations').hide();
    $('body').removeClass('is-no-scroll');
      $('body').css({
        position: 'static',
        top: 'auto',
      });
      $(window).scrollTop(offsetY);
  });



};

app.slider = function () {
  if ($(".js-slider").length) {
    var slickItem;
    slickItem = false;
    var sliderProd = function () {
      if ($(window).width() < 768) {
        if (!slickItem) {
          $(".js-slider").slick({
            arrows: true,
            autoplay: false,
            speed: 800,
            dots: false,
            autoplaySpeed: 3000,
          });
          slickItem = true;
        }
      } else if ($(window).width() > 767) {
        if (slickItem) {
          $('.js-slider').slick('unslick');
          slickItem = false;
        }
      }
    };
    sliderProd();
    $(window).on('load resize', function () {
      sliderProd();
    });
  }
};

app.tabs = function () {
  $('.js-tabs').each(function () {
    var $active, $content, $links = $(this).find('a');
    $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
    $active.addClass('is-active');
    $content = $($active.attr('href'));
    $links.not($active).each(function () {
      $($(this).attr('href')).hide();
    });
    $(this).on('click', 'a', function (e) {
      $active.removeClass('is-active');
      $content.hide();
      $active = $(this);
      $content = $($(this).attr('href'));
      $active.addClass('is-active');
      $content.show();
      e.preventDefault();
    });
  });
};


app.treeView = function () {
  if ($('.list-tree').length) {
    $('.list-tree p > span').click(function () {
      var nextUl = $(this).closest('p').next('ul');
      var closestLi = $(this).closest('li');
      if (!closestLi.hasClass('is-expanded') && !closestLi.hasClass('is-collapsed')) {
        return;
      }
      if (closestLi.hasClass('is-expanded')) {
        closestLi.addClass('is-collapsed').removeClass('is-expanded');
        nextUl.slideUp();
      } else {
        closestLi.addClass('is-expanded').removeClass('is-collapsed');
        nextUl.slideDown();
      }
      return false;
    });
  }
};


$(function () {

  app.init();

});


/* Function load */
$(window).on('load', function () {

  if($('.js-loading').length) {
    setTimeout(function () {
      $('.js-loading').fadeOut(500);
    }, 1500);
    setTimeout(function () {
      $('html').addClass('is-loaded');
    }, 1800);
  }

});